import webpack from 'webpack';
// eslint-disable-next-line import/no-extraneous-dependencies
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import paths from './paths';
import rules from './rules';

module.exports = {
  entry: paths.entryPath,
  module: {
    rules
  },
  resolve: {
    modules: ['src', 'node_modules'],
    extensions: ['*', '.js', '.scss', '.css'],
    alias: {
      "$components": path.resolve(__dirname, "../", "../", "./src/components"),
      "$redux": path.resolve(__dirname, "../", "../", "./src/redux"),
      "$actions": path.resolve(__dirname, "../", "../", "./src/redux/actions"),
      "$helpers": path.resolve(__dirname, "../", "../", "./src/helpers"),
      "$reducers": path.resolve(__dirname, "../", "../", "./src/redux/reducers"),
      "$pages": path.resolve(__dirname, "../", "../", "./src/pages"),
      "$api": path.resolve(__dirname, "../", "../", "./src/api"),
    }
  },
  plugins: [
    new webpack.EnvironmentPlugin(['API_URL']),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // all options are optional
      filename: '[name].css',
      chunkFilename: '[id].css',
      ignoreOrder: false, // Enable to remove warnings about conflicting order
    }),
    new HtmlWebpackPlugin({
      title: 'Reign Test',
      minify: {
        collapseInlineTagWhitespace: true,
        collapseWhitespace: true,
        preserveLineBreaks: true,
        minifyURLs: true,
        removeComments: true,
        removeAttributeQuotes: true
      }
    }),
  ]
};