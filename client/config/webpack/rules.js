const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [    
  {
    test: /\.js$/,    
    exclude: /node_modules/,    
    use: {    
      loader: 'babel-loader'    
    }    
  },    
  {
    test: /\.scss$/,
    use: [
      {
        loader: 'style-loader',
      },
      {
        loader: "css-loader",
        options: {
          sourceMap: true,
          modules: true,
          importLoaders: 2,
        },
      },
      {
        loader: "sass-loader",
        options: {
          sourceMap: true,
        },
      }
    ]
  }, {
    test: /\.css$/,
    use: [{
      loader: 'style-loader',
    }, {
      loader: 'css-loader',
    }],
  }, {    
    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    
    exclude: /node_modules/,    
    loader: 'file-loader'    
  }, {    
    test: /\.(woff|woff2)$/,    
    loader: 'url-loader?prefix=font/&limit=5000'    
  }, {
    test: /\.(png|jpe?g|svg|gif)$/,
    use: [
      {
        loader: 'file-loader',
      },
    ],
  },  
];