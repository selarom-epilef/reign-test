import path from 'path';

module.exports = {    
  root: path.resolve(__dirname, '../', '../'),    
  outputPath: path.resolve(__dirname, '../', '../', 'build'),    
  entryPath: path.resolve(__dirname, '../', '../', 'src/index.js'),      
  imagesFolder: 'images',    
  fontsFolder: 'fonts',    
  cssFolder: 'css',    
  jsFolder: 'js'    
};