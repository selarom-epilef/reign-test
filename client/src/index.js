import React from 'react';
import ReactDOM from 'react-dom';

import Routes from './Routes';

import 'typeface-roboto';

// Make an element to hook react
const container = document.createElement('div');
container.id = 'APP';
document.body.appendChild(container);

ReactDOM.render(
  <Routes />,
  container,
);
