import { postTypes as types } from '$actions';

const initialState = {
  isLoading: true, // Are posts being fetched?
  posts: [], // List of posts
  error: null, // Last error message

  underDeletion: {}, // A hash to store posts being deleted to avoid multiple requests
};

const courseReducer = (state = initialState, action) => {
  switch (action.type) {
  case types.GET_POSTS_REQUEST: {
    return {
      ...state,
      isLoading: true,
      error: null,
    };
  }

  case types.GET_POSTS_SUCCESS: {
    const { posts } = action;
    return {
      ...state,
      posts,
      isLoading: false,
      error: null,
    };
  }

  case types.GET_POSTS_FAILURE: {
    return {
      ...state,
      isLoading: false,
      error: 'Unable to fetch posts. Is the server running?',
    };
  }

  case types.DELETE_POST_REQUEST: {
    const { postId } = action;
    return {
      ...state,
      underDeletion: {
        ...state.underDeletion,
        [postId]: true,
      },
    };
  }
  case types.DELETE_POST_SUCCESS: {
    const { postId } = action;
    return {
      ...state,
      posts: state.posts.filter((post) => post.id !== postId),
      underDeletion: {
        ...state.underDeletion,
        [postId]: false,
      },
    };
  }
  case types.DELETE_POST_FAILURE: {
    const { postId } = action;
    return {
      ...state,
      underDeletion: {
        ...state.underDeletion,
        [postId]: false,
      },
    };
  }
  default: {
    return state;
  }
  }
};

export default courseReducer;
