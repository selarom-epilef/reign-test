import { combineReducers } from 'redux';

import postState from './postReducer';

const rootReducer = combineReducers({
  postState,
});

export default rootReducer;
