import { postTypes as types } from './index';

export const onGetPostsRequest = () => ({
  type: types.GET_POSTS_REQUEST,
});

export const onGetPostsSuccess = (posts) => ({
  type: types.GET_POSTS_SUCCESS,
  posts,
});

export const onGetPostsFailure = (error) => ({
  type: types.GET_POSTS_FAILURE,
  error,
});

export const onDeletePostRequest = (postId) => ({
  type: types.DELETE_POST_REQUEST,
  postId,
});

export const onDeletePostSuccess = (postId) => ({
  type: types.DELETE_POST_SUCCESS,
  postId,
});

export const onDeletePostFailure = (postId, error) => ({
  type: types.DELETE_POST_FAILURE,
  postId,
  error,
});
