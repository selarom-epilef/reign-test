import { applyMiddleware, createStore, compose } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import middleware from './middleware';
import reducer from './reducers';

// Prsisted data
const persistConfig = {
  key: 'root',
  storage,
  version: 1,
  whitelist: ['postsState'],
};

const persistedReducer = persistReducer(persistConfig, reducer);

const store = createStore(
  persistedReducer,
  {},
  compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : (f) => f,
  ),
);

export const persistor = persistStore(store);

export default store;
