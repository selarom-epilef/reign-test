/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import {
  Box, Grid, ListItem, IconButton,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { styled } from '@material-ui/core/styles';

const StyledListItem = styled(ListItem)({
  padding: 0,
});

const StyledBox = styled(Box)({
  flexGrow: 1,
  paddingTop: '1rem',
  paddingBottom: '1rem',
  paddingLeft: '1rem',
  paddingRight: '1rem',
  cursor: 'pointer',
  backgroundColor: '#fff',
  borderBottom: ({ isLast }) => (isLast ? 'none' : '1px solid #ccc'),

  '&:hover': {
    backgroundColor: '#fafafa',
  },
});

const Title = styled('div')({
  color: '#333',
  fontSize: '13pt',
});

const Author = styled('div')({
  color: '#999',
  fontSize: '13pt',
  paddingLeft: '0.5rem',
});

const Time = styled('div')({
  color: '#333',
  fontSize: '13pt',
});

const Post = ({
  post, onDelete, isDeleting, isLast,
}) => (
  <StyledListItem>
    <StyledBox isLast={isLast}>
      <Grid container spacing={0} alignItems="center">
        <Grid item xs={9} container>
          <Title>
            {post.title || post.title || ''}
          </Title>
          <Author>
            {`- ${post.author} -`}
          </Author>
        </Grid>

        <Grid item xs={2}><Time>{moment(post.created_at).fromNow()}</Time></Grid>

        <Grid item xs={1}>
          <IconButton disabled={isDeleting} onClick={() => onDelete(post.id)}>
            <DeleteIcon />
          </IconButton>
        </Grid>
      </Grid>

    </StyledBox>
  </StyledListItem>
);

Post.propTypes = {
  post: PropTypes.shape({
    story_title: PropTypes.string,
    title: PropTypes.string,
  }).isRequired,
  isDeleting: PropTypes.bool.isRequired,
  onDelete: PropTypes.func.isRequired,
  isLast: PropTypes.bool.isRequired,
};

export default Post;
