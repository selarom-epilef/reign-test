import React from 'react';

import { AppBar, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const StyledAppBar = withStyles({
  root: {
    height: '8rem',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: '1.5rem',
  },
})(AppBar);

const NavBar = () => (
  <StyledAppBar>
    <Typography variant="h3" noWrap>
      HN Feed
    </Typography>
    <Typography variant="h6" noWrap>
      {'We <3 hacker news!'}
    </Typography>
  </StyledAppBar>
);

export default NavBar;
