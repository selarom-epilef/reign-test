import axios from 'axios';

import { API_URL } from '$helpers/constants';

import {
  onGetPostsRequest,
  onGetPostsSuccess,
  onGetPostsFailure,
  onDeletePostRequest,
  onDeletePostSuccess,
  onDeletePostFailure,
} from '$actions/postActions';

import store from '$redux/store';

export const getAll = () => {
  store.dispatch(onGetPostsRequest());
  return axios({
    method: 'GET',
    url: `${API_URL}/posts`,
  })
    .then((response) => {
      const posts = response.data;
      store.dispatch(onGetPostsSuccess(posts));
    })
    .catch(() => {
      store.dispatch(onGetPostsFailure('Failed to fetch posts'));
    })
};

export const remove = (postId) => {
  store.dispatch(onDeletePostRequest(postId));

  return axios({
    method: 'DELETE',
    url: `${API_URL}/posts/${postId}`,
  })
    .then(() => {
      store.dispatch(onDeletePostSuccess(postId));
    })
    .catch(() => {
      store.dispatch(onDeletePostFailure(postId, 'Unable to delete the post'));
    });
};
