import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  Typography, List, Grid, Button,
} from '@material-ui/core';
import { styled } from '@material-ui/core/styles';

// Api
// eslint-disable-next-line import/named
import { postsApi } from '$api';

// Components
import Post from '$components/Post';

const Container = styled('div')({
  height: 'calc(100% - 10rem)',
  marginTop: '10rem',
  width: '100vw',
  padding: '2rem',
});

class Home extends PureComponent {
  static propTypes = {
    posts: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    underDeletion: PropTypes.shape({}).isRequired,
  };

  componentDidMount = () => {
    postsApi.getAll();
  }

  onDeletePost = (postId) => {
    postsApi.remove(postId);
  }

  onRefetchPosts = () => {
    postsApi.getAll();
  }

  render() {
    const {
      posts, isLoading, error, underDeletion,
    } = this.props;

    const filteredPosts = posts.filter((post) => post.sub_title || post.title);

    return (
      <Container>
        {isLoading && (
          <Typography variant="p1" noWrap>
            Loading posts...
          </Typography>
        )}
        {error && (
          <Grid xs={12}>
            <Typography variant="p1" noWrap>
              {error}
            </Typography>
            <Button variant="contained" color="primary" onClick={this.onRefetchPosts}>Retry</Button>
          </Grid>
        )}
        <List>
          {filteredPosts.map((post, index) => (
            <Post
              key={post.id}
              post={post}
              isDeleting={underDeletion[post.id]}
              onDelete={this.onDeletePost}
              isLast={index === filteredPosts.length - 1}
            />
          ))}
        </List>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  posts: state.postState.posts,
  isLoading: state.postState.p,
  error: state.postState.error,
  underDeletion: state.postState.underDeletion,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
