import chai from 'chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import 'sinon-mongoose';

import { fetchHackerNews } from '$jobs';
import { hackerNewsService } from '$services';
import Post from '$schemas/Post';

chai.use(chaiHttp);

const { expect } = chai;

describe('jobs', () => {
  describe('fetchHackerNews', () => {
    it('calls hacker news services and does a bulk insert', (done) => {
      const getStub = sinon.stub(hackerNewsService, 'getAll').resolves([{ objectID: 'hello' }]);
      const bulkStub = sinon.stub(Post, 'bulkWrite').resolves(null);
      fetchHackerNews()
        .then(() => {
          expect(getStub.called).to.be.equal(true);
          expect(bulkStub.called).to.be.equal(true);
          getStub.restore();
          bulkStub.restore();
          done();
        })
        .catch((err) => {
          getStub.restore();
          bulkStub.restore();
          done(err);
        });
    });
  });
});
