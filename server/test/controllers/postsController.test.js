import sinon from 'sinon';
import 'sinon-mongoose';
import chai from 'chai';
import chaiHttp from 'chai-http';

import Post from '$schemas/Post';
import app from '$root/app';

chai.use(chaiHttp);

const { expect } = chai;


describe('newsController', () => {
  describe('get /', () => {
    let server;
    before(() => {
      server = app.listen();
    });
    after(() => {
      server.close();
    });

    it('responds with all posts on the database', (done) => {
      const findStub = sinon.stub(Post, 'find').returns([]);
      chai.request(server)
        .get('/posts')
        .end((err, res) => {
          findStub.restore();
          expect(err).to.be.null;
          expect(res.status).to.be.equal(200);
          done();
        });
    });
  });

  describe('delete /:id', () => {
    let server;
    before(() => {
      server = app.listen();
    });
    after(() => {
      server.close();
    });

    it('responds 404 if id is valid but not in database', (done) => {
      const updateStub = sinon.stub(Post, 'updateOne').returns(null);
      const findStub = sinon.stub(Post, 'findOne').throws();
      chai.request(server)
        .delete('/posts/4920349')
        .end((err, res) => {
          findStub.restore();
          updateStub.restore();
          expect(err).to.be.null;
          expect(res.status).to.be.equal(404);
          done();
        });
    });

    it('responds 204 if succesfully deleted', (done) => {
      const updateStub = sinon.stub(Post, 'updateOne').returns(null);
      const findStub = sinon.stub(Post, 'findOne').returns({});
      chai.request(server)
        .delete('/posts/4920349')
        .end((err, res) => {
          findStub.restore();
          updateStub.restore();
          expect(err).to.be.null;
          expect(res.status).to.be.equal(204);
          done();
        });
    });
  });
});
