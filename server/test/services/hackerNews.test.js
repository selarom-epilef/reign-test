import axios from 'axios';
import { expect, assert } from 'chai';
import sinon from 'sinon';


import { HACKER_NEWS_URL } from '$constants/endpoints';
import { hackerNewsService } from '$services';
import { buildRandomPost } from '$test/helpers';


describe('hackerNews', () => {
  describe('getAll', () => {
    it('responds with the list of posts', (done) => {
      expect(HACKER_NEWS_URL).to.not.be.undefined;
      const post = buildRandomPost();
      const axiosStub = sinon.stub(axios, 'get').resolves({
        data: { hits: [post] },
        status: 200,
      });
      hackerNewsService.getAll()
        .then((posts) => {
          axiosStub.restore();
          expect(posts.length).to.equal(1);
          expect(posts[0]).to.equal(post);
          done();
        })
        .catch((err) => {
          axiosStub.restore();
          done(err);
        });
    });

    it('errors gracefully when invalid response is received', (done) => {
      const axiosStub = sinon.stub(axios, 'get').resolves({
        data: 'RUBBISH',
        status: 200,
      });
      hackerNewsService.getAll()
        .then(() => {
          axiosStub.restore();
          assert.fail('malformated responses should not succeed');
        })
        .catch((err) => {
          axiosStub.restore();
          expect(err.message).to.be.equal('Invalid response received');
          done();
        });
    });

    it('handles non-ok response by throwing axios error', (done) => {
      const axiosStub = sinon.stub(axios, 'get').throws({
        message: '404',
      });
      hackerNewsService.getAll()
        .then(() => {
          axiosStub.restore();
          assert.fail('failed responses should not succeed');
        })
        .catch((err) => {
          axiosStub.restore();
          expect(err.message).to.be.equal('404');

          done();
        });
    });
  });
});
