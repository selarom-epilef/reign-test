import Faker from 'faker';

Faker.seed(5000); // Same seed to make it repeatable

export const buildRandomPost = (override = {}) => ({
  created_at: '2020-03-31T21:45:48.000Z',
  title: Faker.lorem.sentence(),
  url: Faker.internet.url(),
  author: Faker.internet.userName,
  points: Faker.random.number,
  story_text: null,
  comment_text: null,
  num_comments: 0,
  story_id: null,
  story_title: null,
  story_url: null,
  parent_id: null,
  created_at_i: new Date(),
  _tags: [
    'story',
    'author_illegalthoughts',
    'story_22742994',
  ],
  objectId: '22742994',
  _highlightResult: {
    title: {
      value: 'GridDB Achieves 5m Writes and 60m Reads Per Second with only 20 <em>Nodes</em> on GCP',
      matchLevel: null,
      fullyHighlighted: false,
      matchedWords: [
        'nodejs',
      ],
    },
    url: {
      value: 'https://griddb.net/en/blog/million-writes-reads-google-cloud/',
      matchLevel: 'none',
      matchedWords: [],
    },
    author: {
      value: 'illegalthoughts',
      matchLevel: 'none',
      matchedWords: [],
    },
  },
  ...override,
});
