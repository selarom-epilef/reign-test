// eslint-disable-next-line import/named
import { hackerNewsService } from '$services';

import Post from '$schemas/Post';

export const fetchHackerNews = async () => {
  console.info('[JOB] Running fetchHackerNews job');
  const posts = await hackerNewsService.getAll();

  console.info(`[JOB] Fetched ${posts.length} post(s)`);

  await Post.bulkWrite(
    posts.map((post) => ({
      updateOne: {
        filter: { externalObjectId: post.bjectID },
        update: {
          $set: {
            externalObjectId: post.objectID,
            content: post,
          },
        },
        upsert: true,
      },
    })),
  );

  console.info('[JOB] Successfully ran fetchHackerNews job');
};
