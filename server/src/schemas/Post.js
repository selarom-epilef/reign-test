import mongoose, { Schema } from 'mongoose';

const PostSchema = new Schema({
  externalObjectId: { type: String, unique: true },
  content: Object,
  isDeleted: { type: Boolean, default: false, index: true },
});

const PostModel = mongoose.model('Post', PostSchema);

export default PostModel;
