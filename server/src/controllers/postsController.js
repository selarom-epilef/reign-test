import Router from 'koa-router';
import Post from '$schemas/Post';


const router = new Router();

router.get('/', async (ctx) => {
  ctx.response.body = { message: 'Hello!' };
  const posts = await Post.find({ isDeleted: { $ne: true } });

  ctx.body = posts.map((post) => ({ ...post.content, id: post._id }));
  return ctx;
});

router.delete('/:id', async (ctx) => {
  const { id } = ctx.params;

  try {
    await Post.findOne({ _id: id });
  } catch (e) {
    ctx.status = 404;
    return;
  }
  await Post.updateOne({
    _id: id,
  }, {
    $set: { isDeleted: true },
  });

  ctx.status = 204;
});

export default router.routes();
