import Router from 'koa-router';

import postsController from './postsController';

const router = new Router();

router.use('/posts', postsController);

export default router.routes();
