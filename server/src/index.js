import mongoose from 'mongoose';
import Agenda from 'agenda';

import { fetchHackerNews } from '$jobs';

import app from './app';

// Connect with the mongo database
mongoose.Promise = Promise;
mongoose.connect(process.env.MONGO_URL);

// Initifalize agenda
const agenda = new Agenda({ db: { address: process.env.MONGO_URL } });

agenda.define('fetch-hacker-news', fetchHackerNews);

// Jobs
// eslint-disable-next-line func-names
(async function () {
  console.info('[AGENDA] Job scheduler is starting');
  await agenda.start();
  await agenda.every('0 * * * *', 'fetch-hacker-news');

  // do it on start up to avoid waiting for an entire hour
  await agenda.now('fetch-hacker-news');

  console.info('[AGENDA] Job scheduler has started');
}());


console.info('Server is up and running!');
app.listen(4000);
