# Reign test
The implementation of both the client and the server described on the requirements

## Stack
React.js was used for the frontend part and Koa (Node.js) fro the backend.

The application is ready to be extended if required, by setting up redux and its components (actions, reducers, store, etc), as well as having react-router ready for use.

### Client
Move to the client's folder

```
  cd client
```

Create an `.env` file from the `.env.example` provided:

```
  cp .env.example .env
```

Run the server

```
  docker-compose up
```

The client will be available at this URL by default: `http://localhost:8080`

### Server
```
  cd server
```

Create an `.env` file from the `.env.example` provided:

```
  cp .env.example .env
```

Run the server

```
  docker-compose up
```

The server will be available at this URL by default: `http://localhost:4040`


#### Running tests
Tests can be run on the server if the server is already running by executing:
```
  docker-compose exec api yarn run test
```

For coverage information, do:
```
  docker-compose exec api yarn run coverage
```